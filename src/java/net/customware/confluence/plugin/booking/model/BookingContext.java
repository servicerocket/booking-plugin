/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.booking.model;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.User;
import com.opensymphony.webwork.ServletActionContext;
import com.thoughtworks.xstream.XStream;

/**
 * @author David
 */
public class BookingContext
{
    /**
     * This is the maximum length that a property may be.
     */
    public static final int MAX_PROPERTY_LENGTH = 255;

    private final transient ContentEntityObject content;
    private final transient ContentPropertyManager manager;
    private final transient XStream xStream;
    private final transient UserAccessor userAccessor;
    
    private String userName;
    
    /**
     * 
     */
    public BookingContext(UserAccessor userAccessor, ContentEntityObject content, ContentPropertyManager manager, XStream xStream)
    {
        this.userAccessor = userAccessor;
        this.content = content;
        this.manager = manager;
        this.xStream = xStream;
    }
    
    public void setStringProperty(String name, String value)
    {
        manager.setStringProperty(content, name, value);
    }
    
    public String getStringProperty(String name)
    {
        return getStringProperty(name, null);
    }
    
    public String getStringProperty(String name, String defaultValue)
    {
        String val = manager.getStringProperty(content, name);
        if (val == null)
            return defaultValue;
        
        return val;
    }

    public void setObjectProperty(String name, Object value)
    {
        if (value != null)
        {
            String valXml = toXML(value);
            if (valXml.length() > MAX_PROPERTY_LENGTH)
                throw new BookingException("The object is too large to be stored!");
            manager.setStringProperty(content, name, valXml);
        }
        else
            manager.setStringProperty(content, name, null);
    }
    
    public Object getObjectProperty(String name)
    {
        return getObjectProperty(name, null);
    }
    
    public Object getObjectProperty(String name, Object defaultValue)
    {
        String valXml = getStringProperty(name);
        
        if (valXml != null)
        {
            return fromXML(valXml);
        }
        
        return defaultValue;
    }
    
    private String toXML(Object value)
    {
        configXStream();
        return xStream.toXML(value);
    }
    
    private Object fromXML(String value)
    {
        configXStream();
        return xStream.fromXML(value);
    }
    
    private void configXStream()
    {
        xStream.alias("booking", Booking.class);
    }
    
    /**
     * Returns the id of the current user.
     * 
     * @return
     */
    public String getUserName()
    {
        final HttpServletRequest request = ServletActionContext.getRequest();
        if (userName == null && request != null && content != null)
        {
            userName = request.getRemoteUser();
        }
        
        return userName;
    }
    
    /**
     * Returns the UserAccessor for the booking context.
     * 
     * @return
     */
    public UserAccessor getUserAccessor()
    {
        return userAccessor;
    }
    
    public User getUser()
    {
        return userAccessor.getUser(getUserName());
    }

    public boolean isAdmin()
    {
        return userAccessor.hasMembership( userAccessor.getGroup(UserAccessor.GROUP_CONFLUENCE_ADMINS), getUser());
    }
}
