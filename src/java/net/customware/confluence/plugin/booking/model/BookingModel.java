/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.booking.model;

import java.util.BitSet;

import org.joda.time.DateMidnight;
import org.joda.time.DateTimeConstants;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.user.User;

/**
 * @author David Peterson
 */
public class BookingModel
{
    public static final BitSet ALL = new BitSet(7);
    public static final BitSet WEEKDAYS = new BitSet(5);
    
    static {
        WEEKDAYS.set(DateTimeConstants.MONDAY);
        WEEKDAYS.set(DateTimeConstants.TUESDAY);
        WEEKDAYS.set(DateTimeConstants.WEDNESDAY);
        WEEKDAYS.set(DateTimeConstants.THURSDAY);
        WEEKDAYS.set(DateTimeConstants.FRIDAY);
        
        ALL.or(WEEKDAYS);
        ALL.set(DateTimeConstants.SUNDAY);
        ALL.set(DateTimeConstants.SATURDAY);
    }
    
    public static final String DATE_FORMAT = "yyyy-MMM-dd";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern(DATE_FORMAT);
    
    /**
     * A one-day period.
     */
    private static final Period DAY = new Period(0, 0, 0, 1, 0, 0, 0, 0);
    /**
     * A one-week period.
     */
    private static final Period WEEK = new Period (0, 0, 1, 0, 0, 0, 0, 0);
    
    private final String name;
    private final String parameterPrefix;
    
    private int firstDay = DateTimeConstants.SUNDAY;
    private BitSet days = (BitSet)ALL.clone();
    
    private String[] items;
    private transient int currentItem;
    
    private transient DateMidnight date;
    private transient Booking booking;
    private transient boolean defrosted = false;
    
    private final transient BookingContext ctx;
    
    /**
     *  
     */
    public BookingModel(String name, String[] items, BookingContext ctx)
    {
        this.name = name;
        this.items = items;
        this.ctx = ctx;
        
        // Default the current item to BOF
        currentItem = -1;
        
        // Default the date to today.
        date = new DateMidnight();
        
        parameterPrefix = "booking-" + name + ".";
    }

    /**
     * Returns the name of the booking list.
     * 
     * @return
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the current date to the specified date.
     * If the date provided is <code>null</code>, the date
     * is set to the actual current date ('now').
     * 
     * @param newDate
     */
    public void gotoDate(DateMidnight newDate)
    {
        if (newDate == null)
            newDate = new DateMidnight();
        
        if (!date.equals(newDate))
        {
            this.date = newDate;
            defrosted = false;
        }
    }
    
    /**
     * Returns the current date.
     * 
     * @return
     */
    public DateMidnight getDate()
    {
        return date;
    }
    
    public String getDateString()
    {
        return DATE_FORMATTER.print(date);
    }
    
    /**
     * Sets the current date to the next day.
     */
    public void gotoNextDay()
    {
        gotoDate(date.plus(DAY));
    }
    
    /**
     * Sets the current date to one day prior.
     *
     */
    public void gotoPrevDay()
    {
        gotoDate(date.minus(DAY));
    }
    
    /**
     * Sets the current date to the start of the current week.
     */
    public void gotoWeekStart()
    {
        int dow = date.getDayOfWeek();
        Period shift = new Period(0, 0, 0, (dow+7-firstDay)%7, 0, 0, 0, 0);
        gotoDate(date.minus(shift));
    }
    
    /**
     * Sets the current date to the end of the current week.
     */
    public void gotoWeekEnd()
    {
        int dow = date.getDayOfWeek();
        Period shift = new Period(0, 0, 0, (6+firstDay-dow)%7, 0, 0, 0, 0);
        gotoDate(date.plus(shift));
    }
    
    /**
     * Sets the booking list to the first day of the previous week
     * from the current date.
     */
    public void gotoPrevWeek()
    {
        gotoWeekStart();
        gotoDate(date.minus(WEEK));
    }
    
    /**
     * Sets the booking list to the first day of the next week from
     * its current date.
     */
    public void gotoNextWeek()
    {
        gotoDate(date.plus(WEEK));
        gotoWeekStart();
    }
    
    public String getItem()
    {
        if (currentItem >=0 && currentItem < items.length)
            return items[currentItem];
        else
            return null;
    }
    
    public int getItemCount()
    {
        return items.length;
    }
    
    /**
     * If the item exists in the booking list, set it to the current item.
     * If not, the current item is unchanged.
     * 
     * @param item The item to set as current.
     */
    public void gotoItem(String item)
    {
        if (item == null)
            return; //do nothing...
        
        int i;
        for (i = 0; i < items.length && !items[i].equals(item); i++);
        
        if (i < items.length)
        {
            currentItem = i;
            defrosted = false;
        }
        else
            throw new IllegalArgumentException("The specified item does not exist: " + item);
    }
    
    public void gotoItem(int index)
    {
        if (index < 0 || index >= items.length)
            throw new IndexOutOfBoundsException("Index is out of bounds: " + index);
        currentItem = index;
    }
    
    /**
     * Sets the list to point at the first item.
     */
    public void gotoFirstItem()
    {
        if (currentItem != 0)
        {
            currentItem = 0;
            defrosted = false;
        }
    }
    
    public void gotoLastItem()
    {
        if (currentItem != items.length-1)
        {
            currentItem = items.length-1;
            defrosted = false;
        }
    }

    /**
     * Returns <code>true</code> if it is currently pointing
     * before the first item. You must call {@link #gotoNextItem()}
     * to have the list pointing at the first item.
     */
    public boolean atBeginningOfItems()
    {
        return currentItem == -1;
    }
    
    /**
     * Returns <code>true</code> if it is currently pointing
     * after the last item. You must call {@link #gotoPrevItem()}
     * to have the list pointing at the last item.
     */
    public boolean atEndOfItems()
    {
        return currentItem > 0;
    }
    
    /**
     * Sets the current item to the previous one.
     * If the current item is already the first item,
     * no change is made, and <code>false</code> is returned.
     */
    public void gotoPrevItem()
    {
        if (currentItem >= 0)
        {
            currentItem--;
            defrosted = false;
        }
    }
    
    public void gotoNextItem()
    {
        if (currentItem < items.length)
        {
            currentItem++;
            defrosted = false;
        }
    }

    /**
     * Returns the current booking, or <code>null</code> if no booking
     * has been made for the current item on the current date.
     * 
     * @return The booking, or <code>null</code>.
     */
    public Booking getBooking()
    {
        if (!defrosted)
        {
		        booking = null;
		
		        if (currentItem >= 0 && currentItem < items.length)
		            booking = (Booking)ctx.getObjectProperty(getBookingPropName());
		            
		        defrosted = true;
        }
	      return booking;
    }
    
    /**
     * Sets the booking details for the current item and date.
     * If a booking already exists, a {@link BookingException} is thrown.
     * 
     * @param booking
     */
    public void setBooking(Booking booking)
    {
        Booking currentBooking = getBooking();
        
        User currentUser = ctx.getUser();
        boolean currentAdmin = ctx.isAdmin(); //currentUser.inGroup(UserAccessor.GROUP_CONFLUENCE_ADMINS);
        
        if (currentUser == null)
            throw new BookingException("Please log in to book or unbook items.");
        
        if (booking != null && currentBooking != null && !currentAdmin)
        {
            String currentUserName = currentUser.getName();
            // Check if the new booker has permission to do so.
            if (!currentUserName.equals(currentBooking.getBookedFor()) 
             && !currentUserName.equals(currentBooking.getBooker()))
                throw new BookingException("A booking already exists for \"" + getItem() + "\" on " + getDate().toString("MMM dd, yyyy"));
        }
        
        /* If a booking already exists and the booking is being cleared, 
         * check that the current user has permission to clear it.
         */
        if (booking == null && currentBooking != null && !currentAdmin)
        {
            User booker = ctx.getUserAccessor().getUser(currentBooking.getBooker());
            
            if (!currentUser.equals(booker))
            {
                throw new BookingException("You do not have permission to cancel the booking for \"" + getItem() + "\" on " + getDate().toString("MMM dd, yyyy") + ".");
            }
        }
        
        defrosted = true;
        this.booking = booking;
        ctx.setObjectProperty(getBookingPropName(), booking);
    }
    
    /**
     * Returns a string containing the parameters required for
     * accessing the current list, item and date in URL-string friendly format.
     * 
     * @return
     */
    public String getUrlString()
    {
        StringBuffer sb = new StringBuffer();

        String item = getItem();
        if (item != null)
            sb.append("&").append(GeneralUtil.urlEncode(getParameterName("item"))).append("=").append(GeneralUtil.urlEncode(getItem()));
        DateMidnight date = getDate();
        if (date != null)
            sb.append("&").append(GeneralUtil.urlEncode(getParameterName("date"))).append("=").append(GeneralUtil.urlEncode(getDateString()));
        
        return sb.toString();
    }
    
    public String getParameterName(String suffix)
    {
        return parameterPrefix + suffix;
    }
    
    private String getBookingPropName()
    {
        return "booking-" + name + "-" + getItem() + "-" + date.toString(DATE_FORMAT);
    }
    
    public boolean isAdministrator(User user)
    {
        return ctx.isAdmin();
        //return user != null && user.inGroup(UserAccessor.GROUP_CONFLUENCE_ADMINS);
    }
    
    /**
     * Returns the value of the first day of the week.
     * Monday = 1, Sunday = 7 (ISO standard).
     * 
     * @return
     */
    public int getFirstDay()
    {
        return firstDay;
    }
    
    /**
     * Sets the value of the first day of the week.
     * Monday = 1, Sunday = 7 (ISO standard).
     * 
     * @param firstDay
     */
    public void setFirstDay(int firstDay)
    {
        this.firstDay = firstDay;
    }
    
    /**
     * Returns <code>true</code> if the current date should be shown.
     */
    public boolean isDateVisible()
    {
        return isDayVisible(date.getDayOfWeek());
    }
    
    private boolean isDayVisible(int day)
    {
        return days.get(day);
    }
    
    /**
     * Returns the number of days that are visible.
     *
     * @return
     */
    public int getDaysVisible()
    {
        return days.cardinality();
    }
    
    public BitSet getDays()
    {
        return days;
    }
    
    public void setDays(BitSet days)
    {
        this.days = days;
    }
}
