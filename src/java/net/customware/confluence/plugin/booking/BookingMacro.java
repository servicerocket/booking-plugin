/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.plugin.booking;

import java.util.BitSet;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.customware.confluence.plugin.booking.model.Booking;
import net.customware.confluence.plugin.booking.model.BookingContext;
import net.customware.confluence.plugin.booking.model.BookingException;
import net.customware.confluence.plugin.booking.model.BookingModel;

import org.joda.time.DateMidnight;
import org.joda.time.DateTimeConstants;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.apache.log4j.Logger;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.RenderContext;
import com.opensymphony.webwork.ServletActionContext;
import com.thoughtworks.xstream.XStream;

/**
 * This macro defines a simple booking list.
 * <p/>
 * Usage:
 * <pre>
 * {booking:Vehicles}
 * Mazda 626
 * Toyota Camrey
 * Mercedes A-Class
 * {booking}
 * </pre>
 * <p/>
 * The booking model is maintained by a separate model class ({@link BookingModel}).
 */
public class BookingMacro extends BaseMacro
{
    private static final String BOOKING_TEMPLATE = "templates/extra/booking/bookingmacro.vm";
    private static final String DETAILS_TEMPLATE = "templates/extra/booking/bookingdetails.vm";
    private static final String COPY_TEMPLATE = "templates/extra/booking/bookingcopy.vm";

    private static final String ALL = "ALL";
    private static final String WEEKDAYS = "WEEKDAYS";

    private static final String WEEKLY = "weekly";
    private static final String DAILY = "daily";

    private static final String SUNDAY = "SU";
    private static final String MONDAY = "MO";
    private static final String TUESDAY = "TU";
    private static final String WEDNESDAY = "WE";
    private static final String THURSDAY = "TH";
    private static final String FRIDAY = "FR";
    private static final String SATURDAY = "SA";

    private static final Logger LOG = Logger.getLogger(BookingMacro.class);

    UserAccessor userAccessor;
    ContentPropertyManager contentPropertyManager;
    XStream xStream;

    public BookingMacro()
    {
        xStream = new XStream();
        xStream.setClassLoader(getClass().getClassLoader());
    }

    public String execute(Map params, String body, RenderContext renderContext) throws MacroException
    {
        PageContext pageContext = (PageContext)renderContext;

        // The message to display on an error.
        String message = null;

        // retrieve the first parameter passed, in our case the name of the task list.
        String listName = RenderUtils.getParameter(params, null, 0);
        int firstDay = convertDay((String) params.get("firstDay"), DateTimeConstants.SUNDAY);
        BitSet days = convertDays((String) params.get("days"));

        // retrieve a reference to the body object this macro is in
        ContentEntityObject contentObject = pageContext.getEntity();

        // get the body of the macro, and create the task list model
        String[] items = body.trim().split("\r\n|\n");

        BookingContext ctx = new BookingContext(userAccessor, contentObject, contentPropertyManager, xStream);
        BookingModel bookings = new BookingModel(listName, items, ctx);
        bookings.setFirstDay(firstDay);
        bookings.setDays(days);

        String templateName = BOOKING_TEMPLATE;

        // check if any request parameters came in to complete or uncomplete tasks
        final HttpServletRequest req = ServletActionContext.getRequest();
        if (req != null && contentObject != null)
        {
            String newDate = getParamValue(req, bookings, "date");
            if (newDate != null)
                bookings.gotoDate(BookingModel.DATE_FORMATTER.parseDateTime(newDate).toDateMidnight());
            bookings.gotoItem(getParamValue(req, bookings, "item"));

            String remoteUser = req.getRemoteUser();

            try
            {
                if (getParamValue(req, bookings, "details") != null)
                {
                    templateName = DETAILS_TEMPLATE;
                }
                else if (getParamValue(req, bookings, "save") != null)
                {
                    templateName = DETAILS_TEMPLATE;
                    saveBooking(bookings, remoteUser, req);
                    templateName = BOOKING_TEMPLATE;
                }
                else if (getParamValue(req, bookings, "saveCopy") != null)
                {
                    templateName = DETAILS_TEMPLATE;
                    saveBooking(bookings, remoteUser, req);
                    templateName = COPY_TEMPLATE;
                }
                else if (getParamValue(req, bookings, "copy") != null)
                {
                    templateName = COPY_TEMPLATE;
                    copyBooking(bookings, req);
                    templateName = BOOKING_TEMPLATE;
                }
                else if (getParamValue(req, bookings, "delete") != null)
                {
                    bookings.setBooking(null);
                }
                else if (getParamValue(req, bookings, "prevWeek") != null)
                {
                    bookings.gotoPrevWeek();
                }
                else if (getParamValue(req, bookings, "nextWeek") != null)
                {
                    bookings.gotoNextWeek();
                }
            }
            catch (BookingException e)
            {
                message = e.getMessage();
            }
        }

        // now create a simple velocity context and render a template for the output
        Map contextMap = MacroUtils.defaultVelocityContext();
        contextMap.put("bookings", bookings);
        contextMap.put("userAccessor", userAccessor);
        contextMap.put("content", contentObject);
        contextMap.put("message", message);

        try
        {
            return VelocityUtils.getRenderedTemplate(templateName, contextMap);
        }
        catch (Exception e)
        {
            LOG.error("Error while trying to display Bookings!", e);
            throw new MacroException(e.getMessage(), e);
        }
    }

    private void saveBooking(BookingModel bookings, String remoteUser, HttpServletRequest req)
    {
        Booking booking = new Booking(remoteUser);
        String bookedFor = getParamValue(req, bookings, "bookedFor");
        if (bookedFor != null && bookedFor.trim().length() == 0)
            bookedFor = null;

        if (bookedFor != null && userAccessor.getUser(bookedFor) == null)
            throw new BookingException("The user '" + bookedFor + "' does not exist. Please choose an existing user.");

        booking.setBookedFor(bookedFor);
        booking.setComments(getParamValue(req, bookings, "comments"));
        bookings.setBooking(booking);
    }

    private void copyBooking(BookingModel bookings, HttpServletRequest req)
    {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");

        // Get the current booking.
        Booking booking = bookings.getBooking();
        DateMidnight originalDate = bookings.getDate();

        try
        {
            DateMidnight startDate = formatter.parseDateTime(getParamValue(req, bookings, "startDate")).toDateMidnight();
            DateMidnight endDate = formatter.parseDateTime(getParamValue(req, bookings, "endDate")).toDateMidnight();
            String copyType = getParamValue(req, bookings, "copyType");

            if (startDate.isAfter(endDate))
                throw new BookingException("The start date (" + startDate.toString("MMMM d, yyyy") + ") is after the end date (" + endDate.toString("MMMM d, yyyy") + ").");

            Period period;
            DateMidnight date;

            if (WEEKLY.equals(copyType))
            {
                // Set the period to one week.
                period = new Period(0, 0, 1, 0, 0, 0, 0, 0);

                // Set the startDate first day that is the same as the current date's day.
                int cdow = bookings.getDate().getDayOfWeek();
                int sdow = startDate.getDayOfWeek();
                date = startDate.plus(new Period(0, 0, 0, 7 + cdow - sdow, 0, 0, 0, 0));
            }
            else if (DAILY.equals(copyType))
            {
                period = new Period(0, 0, 0, 1, 0, 0, 0, 0);
                date = startDate;
            }
            else
            {
                throw new BookingException("A copy operation was requested without providing a valid copy type.");
            }

            while (!date.isAfter(endDate))
            {
                bookings.gotoDate(date);
                if (bookings.getBooking() == null)
                    bookings.setBooking(booking);
                date = date.plus(period);
            }

            bookings.gotoDate(originalDate);
        }
        catch (Exception e)
        {
            throw new BookingException(e.getMessage());
        }
    }

    /**
     * Converts the specified day name string to a number.
     *
     * @param dayName
     * @param defaultValue Returned if <code>dayName</code> is <code>null</code> or unrecognised.
     * @return The integer value for the day (Monday = 1, Sunday = 7)
     */
    private int convertDay(String dayName, int defaultValue)
    {
        if (dayName != null)
        {
            dayName = dayName.toUpperCase();
            if (dayName.startsWith(SUNDAY))
                return DateTimeConstants.SUNDAY;
            else if (dayName.startsWith(MONDAY))
                return DateTimeConstants.MONDAY;
            else if (dayName.startsWith(TUESDAY))
                return DateTimeConstants.TUESDAY;
            else if (dayName.startsWith(WEDNESDAY))
                return DateTimeConstants.WEDNESDAY;
            else if (dayName.startsWith(THURSDAY))
                return DateTimeConstants.THURSDAY;
            else if (dayName.startsWith(FRIDAY))
                return DateTimeConstants.FRIDAY;
            else if (dayName.startsWith(SATURDAY))
                return DateTimeConstants.SATURDAY;
        }

        return defaultValue;
    }

    private BitSet convertDays(String days)
    {
        BitSet daysSet = new BitSet(7);

        if (days != null)
        {
            days = days.trim().toUpperCase();

            if (ALL.equals(days))
            {
                daysSet.or(BookingModel.ALL);
            }
            else if (WEEKDAYS.equals(days))
            {
                daysSet.or(BookingModel.WEEKDAYS);
            }
            else // its a list of days (eg. 'Mon,Tue,Wed')
            {
                String[] daysArray = days.split(" |,|;");
                for (int i = 0; i < daysArray.length; i++)
                {
                    daysSet.set(convertDay(daysArray[i], 0));
                }
            }
        }
        else
        {
            daysSet.or(BookingModel.ALL);
        }

        return daysSet;
    }

    private String getParamValue(HttpServletRequest req, BookingModel bookings, String suffix)
    {
        return req.getParameter(bookings.getParameterName(suffix));
    }

    public String getName()
    {
        return "booking";
    }

    /**
     * This dependency will be resolved automatically before the macro is used.
     */
    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    /**
     * Called to set the UserAccessor for this macro.
     *
     * @param userAccessor
     */
    public void setUserAccessor(UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

}